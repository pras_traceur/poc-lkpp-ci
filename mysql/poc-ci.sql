/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : poc-ci

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 24/02/2021 21:25:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url_api` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of api
-- ----------------------------
BEGIN;
INSERT INTO `api` VALUES (1, 'http://localhost:8000/api/');
COMMIT;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `harga` varchar(255) DEFAULT NULL,
  `stok` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of barang
-- ----------------------------
BEGIN;
INSERT INTO `barang` VALUES (1, 'Laptop 1', 'Laptop nomor 1', '5000000', '10');
INSERT INTO `barang` VALUES (2, 'Komputer', 'Komputer baru', '1000', '10');
INSERT INTO `barang` VALUES (3, 'Meja', 'meja belajar', '500000', '5');
INSERT INTO `barang` VALUES (4, 'Lemari', 'lemari dokumen', '1000000', '2');
INSERT INTO `barang` VALUES (5, 'Monitor', 'monitor LED', '2500000', '15');
INSERT INTO `barang` VALUES (6, 'Mouse', 'mouse kecil', '200000', '20');
INSERT INTO `barang` VALUES (7, 'Server', 'server development', '10000000', '2');
INSERT INTO `barang` VALUES (8, 'Mobil', 'mobil biasa', '100000000', '1');
INSERT INTO `barang` VALUES (9, 'Modem', 'modem 4G', '200000', '30');
INSERT INTO `barang` VALUES (10, 'Router', 'router biasa', '300000', '10');
INSERT INTO `barang` VALUES (11, 'Handphone', 'hp merek lokal', '10000000', '40');
INSERT INTO `barang` VALUES (12, 'Pisang Cokelat', 'Makanan', '20000', '80');
INSERT INTO `barang` VALUES (13, 'Salju', 'dingin', '1000000', '100');
INSERT INTO `barang` VALUES (14, 'Ipad 5', 'Apple Ipad 5', '10000000', '20');
COMMIT;

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `nama_item` varchar(255) DEFAULT NULL,
  `harga` varchar(255) DEFAULT NULL,
  `jumlah_items` varchar(255) DEFAULT NULL,
  `nama_user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
BEGIN;
INSERT INTO `transaksi` VALUES (1, '2020-02-03', 'Laptop 1', '5000000', '1', 'User 2');
INSERT INTO `transaksi` VALUES (2, '2020-02-10', 'Mouse', '200000', '4', 'User 2');
INSERT INTO `transaksi` VALUES (3, '2020-02-07', 'Mouse', '200000', '14', 'User 1');
INSERT INTO `transaksi` VALUES (4, '2020-02-09', 'Handphone', '10000000', '39', 'User 2');
INSERT INTO `transaksi` VALUES (5, '2020-02-03', 'Laptop 1', '5000000', '1', 'Administrator');
INSERT INTO `transaksi` VALUES (6, '2020-02-10', 'Pisang Cokelat', '20000', '20', 'User 10');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `hp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'Administrator', 'Jakarta', '0888888888888', 'admin1@bgs.com');
INSERT INTO `user` VALUES (2, 'User 1', 'Jakarta', '0888888888888', 'user1@bgs.com');
INSERT INTO `user` VALUES (3, 'Karyawan 1', 'Jakarta', '0888888888888', 'karyawan1@bgs.com');
INSERT INTO `user` VALUES (4, 'Manajer 1', 'Jakarta', '0888888888888', 'manajer1@bgs.com');
INSERT INTO `user` VALUES (5, 'dadsa', 'dsadsa', '0811111111111', 'dsadas@ada.com');
INSERT INTO `user` VALUES (6, 'user 8', 'adsadas', '0811111111111', 'asdsa@adsa.com');
INSERT INTO `user` VALUES (7, 'dsadsa', NULL, NULL, 'asdsad@dsa');
INSERT INTO `user` VALUES (8, 'User 10', NULL, NULL, 'user10@gmail.com');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
