<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function getAllDataUser(){
		header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
		$data = [];

		$list = $this->db->query('Select * from `user`')->result_array();

		$no = 1;
		foreach($list as $val){
			array_push($data,array("no" => $no, "name" => $val["name"], "address" => $val["address"], "hp" => $val["hp"], "email" => $val["email"]));
			$no++;
		}

		echo json_encode($data, JSON_PRETTY_PRINT);
	}

    public function getAllDataItem(){
		header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
		$data = [];
		
		$list = $this->db->query('Select * from `barang`')->result_array();

		$no = 1;
		foreach($list as $val){
			array_push($data,array("no" => $no, "nama" => $val["nama"], "deskripsi" => $val["deskripsi"], "harga" => $val["harga"], "stok" => $val["stok"]));
			$no++;
		}

		echo json_encode($data, JSON_PRETTY_PRINT);
	}

    public function getAllDataTransaksi(){
		header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
		$data = [];

		$list = $this->db->query('Select * from `transaksi`')->result_array();

		$no = 1;
		foreach($list as $val){
			array_push($data,array("no" => $no, "tanggal" => date('d F Y', strtotime($val["tanggal"])), "nama_item" => $val["nama_item"], "harga" => $val["harga"], "jumlah_items" => $val["jumlah_items"], "nama_user" => $val["nama_user"]));
			$no++;
		}

		echo json_encode($data, JSON_PRETTY_PRINT);
	}

	public function updateData(){
		$this->db->truncate('user');
		$this->db->truncate('barang');
		$this->db->truncate('transaksi');

		$data1 = [];
		$result1 = $this->getUrl();
		$url1 = $result1["url_api"];
		$list1 = json_decode($this->request($url1."getAllDataUser"));
		foreach($list1 as $val1){
			array_push($data1,array("name" => $val1->name, "address" => $val1->address, "hp" => $val1->hp, "email" => $val1->email));
		}
		$this->db->insert_batch('user', $data1);

		$data2 = [];
		$result2 = $this->getUrl();
		$url2 = $result2["url_api"];
		$list2 = json_decode($this->request($url2."getAllDataItem"));
		foreach($list2 as $val2){
			array_push($data2,array("nama" => $val2->nama, "deskripsi" => $val2->deskripsi, "harga" => $val2->harga, "stok" => $val2->stok));
		}
		$this->db->insert_batch('barang', $data2);

		$data3 = [];
		$result3 = $this->getUrl();
		$url3 = $result3["url_api"];
		$list3 = json_decode($this->request($url3."getAllDataTransaksi"));
		foreach($list3 as $val3){
			array_push($data3,array("tanggal" => $val3->created_date, "nama_item" => $val3->nama_item, "harga" => $val3->harga, "jumlah_items" => $val3->jumlah_items, "nama_user" => $val3->nama_user));
		}
		$this->db->insert_batch('transaksi', $data3);
		
		echo 'sukses';
	}

	function request($url)
	{   
        // inisialisasi CURL
        $data = curl_init();
        // setting CURL
            curl_setopt($data,CURLOPT_URL,$url);
            curl_setopt($data,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($data,CURLOPT_HEADER, false); 
        // menjalankan CURL untuk membaca isi file
        $hasil = curl_exec($data);
        curl_close($data);
        return $hasil;
	}

	function getUrl(){
		$hasil = $this->db->query('Select url_api from `api` where id = 1')->row_array();
		if($hasil){
			return $hasil;
		}else{
			return '';
		}
	}
}
