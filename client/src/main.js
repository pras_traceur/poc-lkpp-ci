import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import router from './router'

Vue.config.productionTip = false

Vue.prototype.$urlci = 'http://18.119.114.62/index.php'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
