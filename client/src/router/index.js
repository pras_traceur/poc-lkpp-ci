import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/user',
    name: 'User',
    component: () => import('../views/User.vue')
  },
  {
    path: '/barang',
    name: 'Barang',
    component: () => import('../views/Barang.vue')
  },
  {
    path: '/belanja',
    name: 'Belanja',
    component: () => import('../views/Belanja.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
